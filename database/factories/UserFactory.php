<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    static  $password;
    return [
        'name' => $faker->name,
        //'email' => $faker->unique()->safeEmail,
        'email' => 'admin@admin.com',
        'email_verified_at' => now(),
        //'password' => $password ?: $password = bcrypt('123456'),
        'password' =>  bcrypt('admin'),
        'remember_token' => str_random(10),
    ];

});

$factory->define(App\Article::class, function (Faker $faker) {

    static $reduce = 999;

    return [
        'title' => $faker->sentence,
        'slug' => $faker->unique()->slug(),
        'body' => $faker->paragraphs($faker->numberBetween(1, 3), true),
        'created_at' => \Carbon\Carbon::now()->subSeconds($reduce--),
    ];
});

$factory->define(App\Tag::class, function (Faker $faker) {

    static $reduce = 999;

    return [
        'name' => $faker->unique()->word,
        'created_at' => \Carbon\Carbon::now()->subSeconds($reduce--),
    ];

});