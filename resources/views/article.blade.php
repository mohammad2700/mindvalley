<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{url('/css/tailwind.min.css')}}" rel="stylesheet">

    <title>Mindvalley | {{$item->title}}</title>


</head>
<body>


<div class="App font-sans min-h-screen flex flex-col min-w-screen bg-grey-lightest">

    <div class="header-wrapper p-4 md:py-4 bg-white border-b border-grey-light">
        <div class="header container max-w-xl mx-auto flex items-center">
            <div class="logo font-bold text-2xl"><a  class="no-underline hover:text-grey-darkest text-blue-dark" href="{{url('/')}}">Mindvalley</a> </div>
            <div class="search flex-1 ml-8 mr-8">
                <form method="get" action="{{url('/search')}}" id="searchForm" >
                    <input name="search"  class="w-full text-grey-dark font-light py-2 bg-grey-lighter px-4 border border-grey-light rounded"
                           value="{{Request('search')}}" placeholder="Search..." type="text" />
                </form>
            </div>
            <div class="hidden sm:block menu ml-auto font-light">
                <a href="{{url('/admin')}}" class="mr-4 no-underline hover:text-grey-darkest text-grey-dark">Admin</a>
                <a href="{{url('/')}}" class="no-underline hover:text-grey-darkest text-grey-dark">Home</a>
            </div>

            <a href="#" class="hamburger sm:hidden text-grey-darkest">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"/><line x1="3" y1="6" x2="21" y2="6"/><line x1="3" y1="18" x2="21" y2="18"/></svg>
            </a>
        </div>
    </div>

    <div class="content-wrapper pt-5 flex-1 bg-grey-lighter">
        <div class="content py-2 container max-w-xl mx-auto ">


                <div class="card md:rounded shadow bg-white p-8 my-5">

                    <div class="container max-w-xl mx-auto pb-10 flex flex-wrap">
                        @foreach ($item->photos as $photo)
                            <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/3 p-3 mb-4" ><img src="{{url('/uploadedfiles',$photo->name)}}" alt="{{$item->title}}" class="w-full h-auto shadow-lg"></div>
                        @endforeach
                    </div>

                    <h1 class="text-3xl">{{$item->title}}</h1>
                    <div class="text-xs text-grey flex items-center my-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="h-4 w-4 mr-1 feather feather-calendar">
                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                            <line x1="16" y1="2" x2="16" y2="6"></line>
                            <line x1="8" y1="2" x2="8" y2="6"></line>
                            <line x1="3" y1="10" x2="21" y2="10"></line>
                        </svg>
                        <span>{{$item->created_at}}</span>
                    </div>

                    <p class="my-4 leading-normal">{!! $item->body !!}</p>

                    <div class="mb-5">
                        @foreach ($item->tags as $tag)
                            <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">{{ $tag->name }}</span>
                        @endforeach
                    </div>



                    <a href="{{url('/')}}" class="no-underline bg-green-light  text-white border border-b-2 border-indigo-dark hover:border-indigo-darker rounded font-semibold px-3 py-2">
                        Back To Home Page
                    </a>
                </div>





        </div>
    </div>



    <div class="footer-wrapper bg-grey-lighter mt-auto pt-8">
        <div class="footer border-t border-grey-light p-4 container mx-auto max-w-md text-grey font-light text-sm tracking-wide flex justify-between">
      <span>
        2019(C) mindvalley .
      </span>
            <a href="https://mindvalley.com" class="no-underline text-grey hover:text-grey-dark">Contact</a>
        </div>
    </div>

</div>


</body>
</html>