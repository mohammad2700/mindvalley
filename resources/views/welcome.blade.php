<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{url('/css/tailwind.min.css')}}" rel="stylesheet">

    <title>Mindvalley Blog</title>


</head>
<body>


<div class="App font-sans min-h-screen flex flex-col min-w-screen bg-grey-lightest">

    <div class="header-wrapper p-4 md:py-4 bg-white border-b border-grey-light">
        <div class="header container max-w-md mx-auto flex items-center">
            <div class="logo font-bold text-2xl text-blue-dark">JS</div>
            <div class="search flex-1 ml-8 mr-8">
                <input class="w-full text-grey-dark font-light py-2 bg-grey-lighter px-4 border border-grey-light rounded" placeholder="Search..." type="text" />
            </div>
            <div class="hidden sm:block menu ml-auto font-light">
                <a href="#" class="mr-4 no-underline hover:text-grey-darkest text-grey-dark">Portfolio</a>
                <a href="#" class="mr-4 no-underline hover:text-grey-darkest text-grey-dark">About</a>
                <a href="#" class="no-underline hover:text-grey-darkest text-grey-dark">Contact</a>
            </div>

            <a href="#" class="hamburger sm:hidden text-grey-darkest">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"/><line x1="3" y1="6" x2="21" y2="6"/><line x1="3" y1="18" x2="21" y2="18"/></svg>
            </a>
        </div>
    </div>

    <div class="content-wrapper pt-8 flex-1 bg-grey-lighter">
        <div class="content py-4 container max-w-md mx-auto ">

            <div class="card md:rounded shadow bg-white p-8">
                <h1 class="text-3xl">Tailwindcss Layout</h1>
                <p class="my-4 leading-normal">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores aperiam atque error quod earum iusto adipisci, molestias excepturi quam, tempora quas provident fugiat ratione sint perferendis veniam non porro omnis.</p>
            </div>

            <div class="card mt-8 md:rounded shadow bg-white p-8">
                <h1 class="text-3xl">Modern CSS</h1>
                <p class="my-4 leading-normal">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores aperiam atque error quod earum iusto adipisci, molestias excepturi quam, tempora quas provident fugiat ratione sint perferendis veniam non porro omnis.</p>
                <button class="bg-indigo text-white border border-b-2 border-indigo-dark hover:border-indigo-darker rounded font-semibold px-3 py-2">
                    read more
                </button>
            </div>

        </div>
    </div>

    <div class="footer-wrapper bg-grey-lighter mt-auto pt-8">
        <div class="footer border-t border-grey-light p-4 container mx-auto max-w-md text-grey font-light text-sm tracking-wide flex justify-between">
      <span>
        2017(C) Mesut Koca.
      </span>
            <a href="https://mesutkoca.com" class="no-underline text-grey hover:text-grey-dark">Contact</a>
        </div>
    </div>

</div>


</body>
</html>