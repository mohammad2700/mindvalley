<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{url('/css/tailwind.min.css')}}" rel="stylesheet">

    <title>Login</title>
    <style>
        .invalid-feedback{color:red}
    </style>

</head>
<body>


<div id="app" class="bg-grey-lighter text-gray-darkest h-screen flex flex-row items-stretch justify-center font-sans">
    <main class="flex w-full md:w-1/2 lg:w-2/5 xl:max-w-md items-stretch md:px-8 lg:px-12 xl:px-16 justify-center flex-col">
        <h1 class="block font-hairline mb-6 text-center lg:max-w-sm mx-auto">Sign In</h1>
        <div class="border-indigo border-t-4 p-8 border-t-12 mb-6">

            <form method="POST" action="{{ route('login') }}">
                @csrf
            <div class="mb-4">
                <label for="email" class="font-bold text-grey-darker block mb-2">Username</label>
                <input type="email" id="email" name="email" class="block appearance-none w-full bg-white border border-grey-light hover:border-grey px-2 py-2 rounded focus:shadow" placeholder="type your email" autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="mb-8">
                <label for="password" class="font-bold text-grey-darker block mb-2">Password</label>
                <input type="password" id="password" name="password" class="block appearance-none w-full bg-white border border-grey-light hover:border-grey px-2 py-2 rounded focus:shadow" placeholder="Password">

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="mb-4 flex items-center justify-between">
                <button class="bg-indigo hover:bg-indigo-dark text-white font-bold py-2 px-4 rounded hover:shadow">Login</button>
                <a href="{{url('/password/reset')}}" class="no-underline hover:underline inline-block align-baseline font-bold text-sm text-indigo hover:text-indigo-dark">Forgot Password?</a>
            </div>

            </form>
        </div>
    </main>
    <aside class="hidden lg:flex flex-1 items-stretch">
        <div class="w-full h-full bg-center bg-no-repeat bg-cover"
             style="background-image: url({{url('/images/team.jpg')}}"></div>
    </aside>
</div>


</body>
</html>