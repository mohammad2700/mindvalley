<?php

namespace App\Http\Controllers\admin;

use App\Article;
use App\Tag;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['items']=Article::latest()->paginate(10);
        return view('admin.articles_list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['tags']=Tag::pluck('name')->ToArray();
        return view('admin.articles_create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleStoreRequest $request)
    {

        $article=Article::Create([
            'title'=>$request->title,
            'slug'=>$request->slug,
            'body'=>$request->body,
        ]);

        $inputTags = $request->tags;
        if ($inputTags && ! empty($inputTags)) {
            $tags = array_map(function($name) {
                return Tag::firstOrCreate(['name' => Tag::generateTagName($name)])->id;
            }, $inputTags);
            $article->tags()->attach($tags);
        }

        $photos = $request['files'];
        if($photos) {
            foreach( $photos as $photo ) {
                if($photo){
                    $article->photos()->UpdateOrCreate([
                        'name'=>$photo,
                    ]);
                }
            }
        }


        return redirect()->route('admin.articles.index')->withSuccess('Article Created');

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $data['tags']=Tag::pluck('name')->ToArray();
        $data['item']=$article;

        return view('admin.articles_edit',$data);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return view
     */
    public function update(ArticleUpdateRequest $request, Article $article)
    {
        $article->update($request->except('_token','tags','files'));

        $inputTags = $request->tags ? : [];
        $tags = array_map(function($name) {
            return Tag::firstOrCreate(['name' => Tag::generateTagName($name)])->id;
        }, $inputTags);
        $article->tags()->sync($tags);


        $photos = $request['files'];
        $article->photos()->delete();
        if($photos) {
            foreach( $photos as $photo ) {
                if($photo){
                    $article->photos()->UpdateOrCreate([
                        'name'=>$photo,
                    ]);
                }
            }
        }


        return redirect()->route('admin.articles.index')->withSuccess('Article Created');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        try {
            if($article->delete()){
                return response()->json([
                    'status' => true,
                    'message' => 'Article Removed Successfully'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => ' There was an error while  deleting the article '
                ]);

            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' =>  $e->getMessage()
            ]);
        }
    }
}
