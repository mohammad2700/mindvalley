<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{url('/css/tailwind.min.css')}}" rel="stylesheet">
    <link href="{{url('/css/toastr.min.css')}}" rel="stylesheet">
    <link href="{{url('/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{url('/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('/css/dropzone.min.css')}}" rel="stylesheet">

    <title>Admin Panel</title>
    <style>
        .pagination {list-style: none; padding: 0; margin: 0}
        .pagination li{display: inline-block; padding: 1rem; border: 0.1rem solid #ccc}
        .pagination li.active{background: #6574cd; border: 0.1rem solid #6574cd;color: #fff;}
        .pagination li a{display: inline-block; text-decoration: none; width: 100%; height: 100%}

        .btn-uploaded-success{
            font-size: 1rem;
            text-align: center;
            display: inline-block ;
            cursor: pointer;
            margin: 0.4rem;
            color: #fff;
            background-color: green;
            padding: 0.4rem;
            border-radius: 0;
            text-decoration: none;
        }
        #dropzoneFileUpload {
            border: 0.5rem solid rgba(0,0,0,0.03);
            min-height: 18rem;
            -webkit-border-radius: 0.5rem;
            background: rgba(0,0,0,0.03);
            padding: 2rem;

        }
        .dropzone .dz-preview .dz-remove {
            font-size: 1rem;
            text-align: center;
            display: inline-block ;
            cursor: pointer;
            margin: 0.4rem;
            color: #fff;
            background-color: #d9534f;
            padding: 0.4rem;
            border-radius: 0;
            text-decoration: none;
        }
        .dropzone .dz-preview.dz-image-preview{padding:0.5rem}


    </style>

</head>
<body>


@include("admin._admin_nav")


<div class="container mx-auto ">
    @include("admin._admin_err")

    @yield("content")
</div>


<script src="{{ url('/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('/js/toastr.min.js') }}"></script>
<script src="{{ url('/js/jquery.blockUI.js') }}"></script>
<script src="{{ url('/js/sw.js') }}"></script>
<script src="{{ url('/js/select2.full.min.js') }}"></script>
<script src="{{ url('/js/dropzone.min.js') }}"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    toastr.options = {
        "progressBar": true,
        "positionClass": "toast-bottom-full-width",
    }
    var baseUrl="{{url('/')}}";
    $(document).on('click','.loading',function() {
        var block_ele = $(this).closest('.card-block');
        block_ele.block({
            message: '<div class="loader-wrapper text-center"><span class="fa-li fa fa-spinner fa-spin fa-lg"></span></div>',
            overlayCSS: {
                backgroundColor: '#FFF',
                cursor: 'wait',
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
    });
</script>
@yield("js")

</body>
</html>