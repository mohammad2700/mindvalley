<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('/uploadFiles', 'DropzoneController@uploadFiles');
Route::get('/uploadedfiles/{filename}', 'HomeController@getUploadedFiles');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/search', 'HomeController@getSearch');
Route::get('/article/{slug}', 'HomeController@viewArtcile');


Route::group(['middleware' => ['auth'], 'prefix' => 'admin','namespace' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'adminController@index');
    Route::resource('articles', 'ArticlesController');
});