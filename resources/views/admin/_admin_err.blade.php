@if ($errors->any())
    <div class="bg-orange-lightest border-l-4 border-orange text-orange-dark p-4 my-4" role="alert">
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif