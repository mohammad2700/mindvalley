<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'body'
    ];

    /**
     * Get all the tags that belong to the article.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'article_tag');
    }

    /**
     * Get all the tags that belong to the article by array.
     *
     * @return array
     */
    public function tagsList()
    {
        return $this->tags()->pluck('name')->toArray() ;
    }

    /**
     * Get article  photos array list .
     *
     * @return array
     */
    public function photosList()
    {
        return $this->photos()->pluck('name')->toArray();
    }

    /**
     * Get all the tags that belong to the article.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function photos()
    {
        return $this->HasMany(Photo::class);
    }

    public function setSlugAttribute($value)
    {
        if(!$value) $value=$this->title;
        $this->attributes['slug'] = str_replace(" ","-",$value);;
    }


    public function getUriAttribute($value)
    {
        return url('/article',$this->slug);
    }



    public function scopeFilter($query=null, $params=null)
    {
        if (isset($params['search']) && trim($params['search']) !== '') {
            $value = trim($params['search']);
            $query->where('title','like','%'. $value.'%')->OrWhere('body','like','%'.$value.'%');
        }

        return $query;

    }


}
