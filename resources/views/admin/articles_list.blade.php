@extends('admin._admin_layout')
@section("content")

    <div class="max-w rounded shadow-lg p-5  my-5 ">
    <a class="inline-block no-underline my-5  bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded" href="{{route('admin.articles.create')}}">Create New Article</a>
    <table class="text-left my-4 w-full " style="border-collapse:collapse">
        <thead>
        <tr>
            <th class="py-4 px-6 bg-grey-lighter font-sans font-medium uppercase text-sm text-grey border-b border-grey-light">title</th>
            <th class="py-4 px-6 bg-grey-lighter font-sans font-medium uppercase text-sm text-grey border-b border-grey-light">created at</th>
            <th class="py-4 px-6 bg-grey-lighter font-sans font-medium uppercase text-sm text-grey border-b border-grey-light"></th>
        </tr>
        </thead>
        <tbody>

        @forelse($items as $item)
        <tr class="hover:bg-blue-lightest card-block">
            <td class="py-4 px-6 border-b border-grey-light"><a target="_blank" href="{{$item->uri}}">{{$item->title}}</a> </td>
            <td class="py-4 px-6 border-b border-grey-light text-center">{{$item->created_at}}</td>
            <td class="py-4 px-6 border-b border-grey-light text-center">
                <a class="inline-block no-underline m-5  bg-blue hover:bg-blue-dark text-white p-2 rounded" href="{{route('admin.articles.edit',$item->id)}}">Edit</a>
                <a class="loading btn-delete inline-block no-underline m-5  bg-red hover:bg-red-dark text-white p-2 rounded"
                   data-id="{{$item->id}}" data-path="{{route('admin.articles.destroy',$item->id)}}" href> Remove </a>
            </td>
        </tr>
        @empty
            <td colspan="3" class="py-4 px-6 border-b border-grey-light">There is nothing to show here right now...</td>
        @endforelse

        </tbody>
    </table>
    </div>
@endsection()


@section('js')
    <script>
        $(document).on('click','.btn-delete',function(e) {
            e.preventDefault();
            var block_ele = $(this).closest('.card-block');
            var btn = $(this);
            var id = $(this).attr('data-id');
            var path = $(this).attr('data-path');

            swal({
                title: 'Warning',
                text: 'You are removing an article !',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes Remove it '
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url :path,
                        type: "DELETE",
                        data:{id:id},
                    }).done(function (response) {
                        if(response.status){
                            toastr.success(response.message);
                            btn.closest("tr").remove();
                        }
                        else
                            toastr.error(response.message);

                    }).fail(function (xhr, status, error) {
                        console.log(error);
                        toastr.error(error);
                    });

                }
            })

            block_ele.unblock();


        });


    </script>
@endsection()