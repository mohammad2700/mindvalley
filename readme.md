
## Installation
edit .env file and make the required configuration for db

     nano .env

Install all the dependencies using composer

    composer install

Run the database migrations

    php artisan migrate
  
Run the database seeder and you're done

    php artisan db:seed
    default user is: admin@admin.com with password : admin



***Note*** : set permissions to storage folder for uploading photos


    chmod -R 775 storage    
        
## Build a Medium.com clone with these features:

- Admin panel to post articles.
- Ability to add one or more photos to the article.
-  Tag Articles
-  Frontend to list and display article.
-  Tag Articles
## Your application should consist of the followings :
-  Database migrations and seeds (Use Faker library to create dummy data)
-  CRUD and Resource Controllers
-  Form Validation and Requests
-  Use of VueJS / Jquery on frontend
-  Tailwindcss as a CSS framework.
-  Testing

 