@extends('admin._admin_layout')
@section("content")

    <a class="inline-block  my-5 no-underline bg-transparent hover:bg-blue text-blue-dark font-semibold hover:text-white py-2 px-4 border border-blue hover:border-transparent rounded" href="{{route('admin.articles.index')}}">Back To Articles List</a>

    <div class="w-full max-w card-block">

        <form id="form1"  method="POST" action="{{ route('admin.articles.store') }}" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            @csrf
            <div class="mb-12">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="title">
                   title *
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                       value="{{old('title')}}" name="title" id="title" type="text" placeholder="Title">
            </div>

            <div class="mb-12">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="slug">
                    slug
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline"
                       value="{{old('slug')}}" id="slug" name="slug" type="text" placeholder="Slug">
                <p class="text-dark py-3 text-xs italic">if empty , we will generate slug based on title attribute </p>
            </div>

            <div class="mb-12">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="tags">
                    Tags
                </label>
                <select multiple="true" name="tags[]" id="tags" class="select2 shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline">
                    @foreach($tags as $tag)
                    <option {{old('tags') && in_array($tag,old('tags')) ? 'selected' : ''}}  value="{{$tag}}">{{$tag}}</option>
                    @endforeach
                </select>
                <p class="text-dark py-3 text-xs italic">type new tags and press Enter </p>
            </div>

            <div class="mb-12">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="photos">
                    Photos
                </label>

                <div class="upload-div">
                    <div class="dropzone" id="dropzoneFileUpload"></div>
                </div>
            </div>


            <div class="mb-12">
                <label class="block text-grey-darker text-sm font-bold mb-2" for="body">
                    Text Body *
                </label>
                <textarea name="body" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-3 leading-tight focus:outline-none focus:shadow-outline" id="body" name="body" rows="5">{{old('body')}}</textarea>

            </div>
            <div class="flex items-center justify-between">
                <button class="loading bg-blue hover:bg-blue-dark text-white font-bold py-4 px-10 rounded focus:outline-none focus:shadow-outline" type="submit">
                  Post
                </button>

            </div>
        </form>

    </div>

@endsection()


@section('js')
    <script>

        $('#tags').select2({
            tags: true,
            tokenSeparators: [",", " "]
        });

        $('#form1').on('submit', function(e){
            var block_ele = $(this).closest('.card-block');
            var title = $('#title').val();
            var body = $('#body').val();
            var err="";

            if(title.length <1 ){
                err +="Please fill the title"+"<br/>";
            }
            if(body.length <1 ){
                err +="Please fill the body "+"<br/>";
            }

            if(err!=""){
                block_ele.unblock();
                toastr.error(err);
                return false;
            }

        });


        //  dropzoneFileUpload
        var maxFilesize=5 ;//mb;
        var Message ="Drag Your Photos Here , Or click inside this box for select from your computer";
        Message +="<br/>";
        Message +=" max size for each file is   "+maxFilesize+" mb ";
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#dropzoneFileUpload", {
            url: baseUrl+"/uploadFiles",
            addRemoveLinks: true,
            dictCancelUpload: "Cancel",
            dictRemoveFile: "Remove",
            acceptedFiles: "image/*",
            autoProcessQueue: true,
            maxFilesize: maxFilesize, // MB
            //parallelUploads: 40,
            // uploadMultiple: true,
            dictDefaultMessage: Message,
            params: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                tp: 'uploadFiles'
            },

            success: function( file, response ){
                var customButton = Dropzone.createElement('<a href>uploaded </a>');
                customButton .setAttribute("class", "btn-uploaded-success");
                file.previewTemplate.appendChild(customButton );
                file.previewElement.addEventListener("dblclick", function() {
                    window.open(baseUrl+'/uploadedfiles/'+response, '_blank');
                });

                $("#form1").append($('<input type="hidden" ' +
                    'name="files[]" ' +
                    'value="' + response + '">'));
            },
            removedfile: function(file) {
                var _ref;
                $('input[value="'+file.name+'"]').remove();
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
        });

    </script>
@endsection()