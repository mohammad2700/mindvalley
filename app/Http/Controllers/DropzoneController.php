<?php

namespace App\Http\Controllers;

 
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Validator;
use Response;

class DropzoneController extends Controller {


    public function uploadFiles() {

       $input = Input::all();

        if (Input::has('tp') && (Input::get('tp')=='uploadFiles') ) {
            $destinationPath= storage_path().'/uploadedfiles/';
            $name = Input::file('file')->getClientOriginalName(); // getting file name
            $extension = Input::file('file')->getClientOriginalExtension(); // getting file extension
            $fileName = md5(microtime()) .".". $extension;// renameing image
            $fullfilename=$destinationPath."/".$fileName;
            $upload_success = Input::file('file')->move($destinationPath, $fileName);

            if ($upload_success) {
                return Response::json( $fileName , 200);
            } else {
                return Response::json('error', 400);
            }
        }

    }

}
