<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Article;


class Tag extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get all the articles that belong to the tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    /**
     * when the user submits these tags, we need to assume that the tag has issues in formatting. We don’t want symbols or spaces, we want everything to be lower case, and we don’t want the tag to start or end with a - or whitespace
     *
     * @return string
     */
    public static function generateTagName( $tagName ){
        /*
          Trim whitespace from beginning and end of tag
        */
        $name = trim( $tagName );

        /*
          Convert tag name to lower.
        */
        $name = strtolower( $name );

        /*
          Convert anything not a letter or number to a dash.
        */
        $name = preg_replace( '/[^a-zA-Z0-9]/', '-', $name );

        /*
          Remove multiple instance of '-' and group to one.
        */
        $name = preg_replace( '/-{2,}/', '-', $name );
        /*
          Get rid of leading and trailing '-'
        */
        $name = trim( $name, '-' );

        /*
          Returns the cleaned tag name
        */
        return $name;
    }
}
