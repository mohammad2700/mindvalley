<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory('App\User', 1)->create();
        //factory('App\Article', 10)->create();
         factory('App\Tag', 10)->create();

        factory(App\Article::class, 5)->create()->each(function ($article) {
            $article->tags()->save(factory(App\Tag::class)->make());
        });
    }
}
