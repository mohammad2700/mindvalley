<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Article;


class Photo extends Model
{
    public $timestamps = false;
    protected $table  = 'article_photo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get all the article that belong to the photo.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function article()
    {
        return $this->belongsTo(Article::class);
    }




}
