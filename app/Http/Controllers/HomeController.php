<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the blog homePage.
     *
     * @return  view/home.blade
     */
    public function index()
    {
        $data['items']=\App\Article::latest('id')->paginate(10);
        return view('home',$data);
    }

    /**
     * Show the article .
     *
     * @return view/article.blade
     */
    public function viewArtcile($slug)
    {
        $data['item']=\App\Article::With('tags')->With('photos')->Where('slug',$slug)->first();
        if(!$data['item']) abort(404);
        return view('article',$data);
    }
    /**
     * Show the search results .
     *
     * @return view/home.blade
     */
    public function getSearch(Request $request)
    {

        $data['items']=\App\Article::Filter($request->all())->paginate(30);
        $data['search']=$request->search;
        return view('home',$data);
    }

    /**
     * Load file in uploadedfiles folder .
     *
     * @return  uploadedfiles/file
     */
    public function getUploadedFiles($filename)
    {
        $path=storage_path('uploadedfiles/').$filename;
        if (!file_exists($path)) {
            abort(404);
        }
        $file = \File::get($path);
        $response = \Response::make($file, 200);
        $response->header("Content-Type",'');
        return $response;
    }


}
